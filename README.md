Download the JSON file "BPDTS_Test.postman_collection" to your local machine.
The collection JSON - contains all our test scripts, URL, Headers, Prerequisite Script etc from postman in JSON format.

Tests can be run in two ways.

1. Using Postman 

Postman a Chrome app is for interacting with HTTP APIs. Postman allows user to automate test cases in javascript with salient features like write test suites, build requests that can contain dynamic parameters, pass data between requests, etc.
The main benefit of using postman is that user does not need to create a full JSON request programmatically unlike other automation API frameworks to put assert on it. Postman beautifully designs them and helps user directly define test cases.

Installing Postman from Chrome
. Visit https://chrome.google.com/webstore/category/extensions
. Enter “Postman Chrome” in “Search the store” section
. In App section postman will be listed
. Click on “ADD TO CHROME”

Postman’s Collection Runner lets you run all requests inside a Postman collection one or more times. It also executes tests and generates reports so that API tests can be compared with previous runs.

Steps to run test cases from Postman :-
• Open Postman
• Click on import button on top left menu and import the collection JSON "BPDTS_Test.postman_collection"
• To run the collection, click on the > button and hit run. 
. This opens the Collection Runner window , runs all tests and generates report, which can be exported to any location..


2. From Command line using Newman

Newman is a companion tool for postman that runs Collections from the command line.

Instructions to install the latest version of Newman on your system:

Install NodeJS – https://nodejs.org/download/. Click on the 32-bit or 64-bit Windows Installer package, depending on your machine configuration.
Add the Node executable to your system path. Go to the Control Panel > System and Security > System > Advanced System Settings > Environment variables. Append this to the end of the PATH variable: ;C:\Program Files\Nodejs
If you installed Node in a different location, you’ll need to set the PATH accordingly.
Install Python 2.x. The latest 2.x release available is 2.7.9 (https://www.python.org/downloads/release/python-279/). Get the Windows x86-64 MSI installer or Windows x86 MSI installer, and follow the instructions.

Open a command prompt, and type “node”. The node environment should start. Press Ctrl-C to exit.
You’ll need to install the Visual Studio runtime for some Newman dependencies to be installed. The quickest way to do this is to install Visual Studio Express – http://go.microsoft.com/?linkid=9816758. 
To check for any additional requirements for your system, check https://github.com/TooTallNate/node-gyp (Windows).
Type “npm install -g newman”. It should take a few minutes to install. 

We need to pass the JSON file "BPDTS_Test.postman_collection" to Newman to run these scripts  from the command line. The basic idea of running these tests using command line is that we can pass them to Jenkins further, which in return will run the test cases periodically.

Steps to run test cases from command line :-
• Open CMD
• Navigate to the folder where JSON is saved
• Execute the Run command as below

"newman run BPDTS_Test.postman_collection.json --reporters cli,html"

In above command --reporters cli,html represents output as console and
html reports.

As soon as the execution is completed, Newman will create an execution report in same folder.

For more details refer below link:-
https://www.getpostman.com/docs/postman/collection_runs/command_line_integration_with_newman

Further possible Extensions are :
 To create different environments like Development, QA, Production etc and run collections against specific environments.
 To pass data files into a run in CSV or JSON formats 
 To pass number of iterations for collection run. Collections can be run multiple times against different data sets to build workflows.
 To pass interval delay (milliseconds) between each request.



